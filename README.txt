The aim of this module is to improve Book outline form to use custom view 
exposed filters to create new book pages.

As your book collection grows it becomes very difficult to look for the correct 
parent book in current outline form, this module tries to solve these situations
 and allows you to add any kind of filter for your books content.

This module will allow you to filter your books select element and expose the 
filters in the form in addition it allows you to add grouping fields in the 
select element.


DEPENDENCIES
--------------------------------------------------------------------------------
This module depends on book module and Views.

INSTALLATION
--------------------------------------------------------------------------------
Enable module in modules list.

CONFIGURATION
--------------------------------------------------------------------------------
Create a new view of type node and add a new block display, enable Ajax for this 
display. 

Add a "depth" filter and set its value to 1, this will limit only to root book 
nodes your results, if you don't add it module will add it for you but for best 
performance add it to your view!. Add any filters to your display if you want 
to allow users to change them just use exposed. If you want to group results 
just add a hidden field (it is not necessary to be hidden but recommended) and 
save your view.

Go to "/admin/content/book/settings" and enable views book, select your view 
and display. 

Note: 
- If you don't enable ajax in the view display, filters will not be applied!
- Only view node types will be shown and only block displays are available.
- You SHOULD add "depth" filter, module will add it at runtime but a message 
will be displayed to warning you and will register that in watchlog.


Hope You enjoy that module.

Please report any bug or feature you think might be 
useful at https://drupal.org/project/issues/2182941.
