<?php
/**
 * @file
 * This module provides views functionality to book outline form widget
 * Please contribute!
 */

/**
 * Implements hook_form_alter().
 */
function viewsbook_form_alter(&$form, &$form_state, $form_id) {
  // Edit admin form to allow view selection.
  if ($form_id == "book_admin_settings") {
    $form['book_viewsbook'] = array(
      '#title' => t('Views book'),
      '#description' => t('Allows filtering book outline form with exposed filters from a view.'),
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#prefix' => '<div id="viewsbook">',
      '#suffix' => '</div>',
    );
    $form['book_viewsbook']['viewsbook_enable'] = array(
      '#title' => t('Enable Views book'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('viewsbook_enable', 0),
      '#ajax' => array(
        'callback' => 'viewsbook_ajax_enable_callback',
        'wrapper' => 'viewsbook',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
    // Check if views book is enabled in form state.
    $viewsbook_enabled = isset($form_state['values']['viewsbook_enable']);
    // If state is not enabled check default value.
    if ($viewsbook_enabled) {
      $viewsbook_enabled = $form_state['values']['viewsbook_enable'] === 1;
    }
    else {
      $viewsbook_enabled = (variable_get('viewsbook_enable', 0) === 1);
    }
    if ($viewsbook_enabled) {
      $default_view = variable_get('book_viewsbook_view', 'none');
      // Get current enabled views with node as base table.
      $views = array_merge(array(
        'none' => '<none>',
      ), drupal_map_assoc(array_keys(array_filter(views_get_enabled_views(), 'viewsbook_view_is_node'))));
      // If there are no views return warning.
      if (count($views) === 0) {
        $form['book_viewsbook']['viewsbook_enable']['#value'] = 0;
        $form['book_viewsbook']['book_viewsbook_error'] = array(
          '#markup' => "<div id='message'><div class='messages error'>" . t('No node views are available. Please create a view of type node and a block display for it') . ".</div></div>",
        );
        return;
      }
      $form['book_viewsbook']['book_viewsbook_view'] = array(
        '#title' => t('Select a view'),
        '#type' => 'select',
        '#options' => $views,
        '#default_value' => $default_view,
        '#required' => TRUE,
        '#ajax' => array(
          'callback' => 'viewsbook_ajax_viewselect_callback',
          'wrapper' => 'viewsbook',
          'method' => 'replace',
          'effect' => 'fade',
        ),
      );
      $issetbvv = isset($form_state['values']['book_viewsbook_view']);
      $bvvnnone = ($issetbvv) ? $form_state['values']['book_viewsbook_view'] != 'none' : FALSE;
      if ($default_view != 'none' || $issetbvv && $bvvnnone) {
        // Process all views display (for now only block).
        if (isset($form_state['values']['book_viewsbook_view'])) {
          $view = views_get_view($views[$form_state['values']['book_viewsbook_view']]);
        }
        else {
          $view = views_get_view($views[$default_view]);
        }
        if (isset($view)) {
          $displays = drupal_map_assoc(array_keys(array_filter($view->display, 'viewsbook_view_display_is_block')));
        }
        else {
          $displays = array();
        }
        $default_display = variable_get('book_viewsbook_display', '');
        $form['book_viewsbook']['book_viewsbook_display'] = array(
          '#title' => t('Select a display'),
          '#type' => 'select',
          '#options' => $displays,
          '#required' => TRUE,
          '#default_value' => $default_display,
          '#disabled' => empty($displays),
          '#description' => t('Please select a view.'),
        );
      }
    }
  }
  if (($form_id == "book_node_form" || $form_id == "book_outline_form") && variable_get('viewsbook_enable', '0') === 1) {
    $view_name = variable_get('book_viewsbook_view', 'none');
    $view_display = variable_get('book_viewsbook_display', 'none');
    if ($view_name != 'none' && $view_display != 'none') {
      // Get position of bid element.
      $pos = array_search('bid', array_keys($form['book']));
      // Load view.
      $view = views_get_view($view_name);
      $view->set_display($view_display);
      // Check if book depth filter is present.
      $depth_filter = $view->get_item($view_display, 'filter', 'depth');
      if (!isset($depth_filter)) {
        $message = t('Your view is not filtering by book depth = 1, added automatically. Consider adding this filter to your view display configuration');
        // Show the message to the user only once.
        if (empty($_SERVER['HTTP_X_REQUESTED_WITH'])) {
          drupal_set_message($message, 'warning', FALSE);
        }
        // Log the warning, so user modifies the view.
        watchdog('viewsbook', $message, array(), WATCHDOG_WARNING, NULL);
        $view->display_handler->default_display->options['filters']['depth']['id'] = 'depth';
        $view->display_handler->default_display->options['filters']['depth']['table'] = 'book_menu_links';
        $view->display_handler->default_display->options['filters']['depth']['field'] = 'depth';
        $view->display_handler->default_display->options['filters']['depth']['value']['value'] = '1';
      }
      $view->init_handlers();
      // Check book depth is limited to 1 (only top books are allowed).
      if (isset($form_state['values']['book']['exposed'])) {
        $view->set_exposed_input($form_state['values']['book']['exposed']);
      }
      $exposed_filters = array(
        'view' => $view,
        'display' => $view->display_handler->display,
        'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'),
        'method' => 'get',
        'rerender' => TRUE,
        'no_redirect' => TRUE,
      );
      // Render exposed filters view form.
      $exposed_rendered = drupal_build_form('views_exposed_form', $exposed_filters);
      $elms = array(
        '#title',
        '#type',
        '#options',
        '#default_value',
        '#multiple',
        '#required',
        '#description',
        '#disabled',
        '#prefix',
        '#suffix',
        '#collapsed',
        '#collapsible',
        '#description',
      );
      // Filters fieldset.
      $filters = array(
        '#type' => 'fieldset',
        '#title' => t('Filters'),
        '#description' => t('Filter book search.'),
        '#weight' => -10,
      );
      // Remove unnecessary elements and add ajax.
      foreach ($exposed_rendered['#info'] as $value) {
        if ($value['label'] != '') {
          $filters[$value['value']]['#title'] = $value['label'];
        }
        foreach ($exposed_rendered[$value['value']] as $key2 => $value2) {
          if (in_array($key2, $elms)) {
            $filters[$value['value']][$key2] = $value2;
          }
        }
        $filters[$value['value']]['#ajax'] = array(
          'callback' => 'viewsbook_ajax_view_callback',
          'wrapper' => 'edit-book-bid-wrapper',
          'effect' => 'fade',
          'speed' => 'fast',
        );
      }
      // Execute view and load matching nodes.
      $view->execute();
      // If node is not yet a book add default options
      // (<none>, create new book).
      if ($form['#node']->book['mlid'] == 0) {
        $form['book']['bid']['#options'] = array_slice(
            $form['book']['bid']['#options'], 0, 2
            );
      }
      else {
        $form['book']['bid']['#options'] = array();
      }
      if (count($view->style_plugin->options['grouping']) > 0) {
        $index = 0;
        foreach ($view->result as $value) {
          // REVIEW: Grouping values from views. How to group by field,
          // there could be more than 1 grouping value, get only first one.
          $grouping_field = $view->style_plugin->options['grouping'][0]['field'];
          $group = filter_xss($view->style_plugin->get_field($index++, $grouping_field), array());
          $form['book']['bid']['#options'][$group][$value->nid] = $value->node_title;
        }
      }
      else {
        foreach ($view->result as $value) {
          $form['book']['bid']['#options'][$value->nid] = $value->node_title;
        }
      }
      // Add wrapper to bid element.
      $form['book']['bid'] = array_merge($form['book']['bid'], array(
        '#prefix' => '<div id="edit-book-bid-wrapper">',
        '#suffix' => '</div>',
      ));
      $form['book'] = array_merge(array_slice($form['book'], 0, $pos - 2), array(
        'exposed' => $filters,
      ), array_slice($form['book'], $pos - 2));
    }
  }
}
/**
 * Ajax Update handler. Returns the new list of books to select.
 */
function viewsbook_ajax_view_callback($form, $form_state) {
  return $form['book']['bid'];
}
/**
 * Ajax Update handler. Returns Views displays list.
 */
function viewsbook_ajax_viewselect_callback($form, $form_state) {
  return $form['book_viewsbook'];
}
/**
 * Ajax Update handler. Returns Views list to select in book admin form.
 */
function viewsbook_ajax_enable_callback($form, $form_state) {
  return $form['book_viewsbook'];
}
/**
 * Check if view is type node.
 *
 * @return bool     
 *   True if view is node type.
 */
function viewsbook_view_is_node($view) {
  return $view->base_table === 'node';
}
/**
 * Check if view display is block type.
 *
 * @return bool        
 *   True if view display is block type.
 */
function viewsbook_view_display_is_block($display) {
  return $display->display_plugin === 'block';
}
